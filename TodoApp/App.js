import { Text, View } from 'react-native';
import { useFonts } from 'expo-font'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import TodosScreen from './screens/TodosScreen';
import SelectedTodoScreen from './screens/SelectedTodoScreen';
import { useEffect, useState } from 'react';
import { getTableInfo, initDB } from './utils/db';
import NativeScreen from './screens/NativeScreen'
import ApiScreen from './screens/ApiScreen';

export default function App() {

  const [loaded, error] = useFonts({
    'nunito-italic': require('./assets/fonts/Nunito-Italic-VariableFont_wght.ttf'),
    'nunito-variable': require('./assets/fonts/Nunito-VariableFont_wght.ttf')
  })

  const [dbInited, setDbInited] = useState(false)

  useEffect(() => {
    initDB()
      .then(res => {
        console.log(res)
        return getTableInfo()
      })
      .then(res => {
        console.log(res.rows._array.map(row => `${row.cid} ${row.name} ${row.type}`))
        setDbInited(true)
      })
  }, [])

  if (!loaded || !dbInited)
    return <View><Text>Hej</Text></View>

  const Stack = createNativeStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='apiscreen'>
        <Stack.Screen name='apiscreen' component={ApiScreen} />
        <Stack.Screen options={{ headerShown: false }} name='todosscreen' component={TodosScreen} />
        <Stack.Screen name='selectedtodoscreen' component={SelectedTodoScreen} />
        <Stack.Screen name='nativescreen' component={NativeScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
