import { Button, View, Alert } from "react-native";
import { Audio } from 'expo-av'
import { useEffect, useState } from "react";

const AudioRecorder = () => {

    const [status, requestPermission] = Audio.usePermissions()
    const [recording, setRecording] = useState()

    useEffect(() => {
        const getPermssion = async () => {
            console.log("statusRecording", status)
            if (status?.granted === false) {
                await requestPermission()
            }
            if (status?.canAskAgain === false) {
                Alert.alert("Insufficient audiorecorder permissions")
            }
        }
        getPermssion()
    }, [status])

    const startRecording = async () => {
        const { recording } = await Audio.Recording.createAsync()
        setRecording(recording)
    }

    const stopRecording = async () => {
        await recording.stopAndUnloadAsync()
        console.log("audio uri:", recording.getURI())
        const soundobject = await recording.createNewLoadedSoundAsync()
        await soundobject.sound.playAsync()
        setRecording(undefined)
    }

    return (
        <View>
            <Button
                title={recording ? "Stop recording" : "Start recording"}
                onPress={recording ? stopRecording : startRecording}
            />
        </View>
    )

}

export default AudioRecorder;