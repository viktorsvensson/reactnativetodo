import { StyleSheet, Text } from "react-native"
import BaseTextStyle from "./BaseTextStyle"

const BaseText = ({ children, inheritedStyle }) => {

    const composedStyle = StyleSheet.compose(
        BaseTextStyle.textStyle,
        inheritedStyle
    )

    return (
        <Text
            style={composedStyle}
        >
            {children}
        </Text>
    )

}

export default BaseText;