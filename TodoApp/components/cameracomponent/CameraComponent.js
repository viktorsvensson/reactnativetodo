import { Camera } from "expo-camera";
import { useEffect, useRef } from "react";
import { Alert, Button, View } from "react-native"

const CameraComponent = () => {

    const [status, requestPermission] = Camera.useCameraPermissions()
    const camRef = useRef()


    useEffect(() => {

        const getPermssion = async () => {

            console.log("statusCamera", status)

            if (status?.granted === false) {
                await requestPermission()
            }
            if (status?.canAskAgain === false) {
                Alert.alert("Insufficient camera permissions")
            }
        }

        getPermssion()

    }, [status])

    const handleSnap = async () => {
        const options = { exif: false, quality: 0.2 }
        const res = await camRef.current.takePictureAsync(options)
        console.log("picture result", res)
    }

    if (status == null) {
        return <View />
    }

    return (
        <View>
            <Camera onCameraReady={() => console.log('ready')} ref={camRef} useCamera2Api style={{ height: 320, width: 180 }}>

            </Camera>
            <Button title='Snap!' onPress={handleSnap}></Button>
        </View>
    )
}

export default CameraComponent;