import { StyleSheet, Text } from "react-native";
import BaseText from "../basetext/BaseText";
import HeaderStyle from "./HeaderStyle";

const Header = () => {

    return (
        <BaseText
            inheritedStyle={HeaderStyle.header}>
            Todo App 3000
        </BaseText>
    )
}

export default Header;