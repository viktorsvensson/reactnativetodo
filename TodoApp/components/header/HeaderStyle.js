import { StyleSheet } from "react-native"

export default StyleSheet.create({
    header: {
        fontSize: 40,
        marginTop: 50
    }
})