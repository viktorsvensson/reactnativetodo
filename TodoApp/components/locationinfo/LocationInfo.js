import { View, Text, Alert, Linking } from "react-native"
import * as Location from 'expo-location';
import { useEffect, useState } from "react";



const LocationInfo = () => {

    const [status, requestPermission] = Location.useForegroundPermissions()
    const [location, setLocation] = useState()

    useEffect(() => {

        const getLocation = async () => {
            if (status?.granted === false && status?.canAskAgain !== false) {
                await requestPermission()
            } else if (status?.granted === true) {
                const loc = await Location.getCurrentPositionAsync();
                setLocation(loc)
            } else if (status != null) {
                //Alert.alert('Insufficent permissions for location, please allow manually')
                Linking.openSettings()
            }
        }

        getLocation();

    }, [status])

    if (status === null) {
        return <View />
    }

    return (
        <View>
            <Text>Lat: {location?.coords.latitude} Long: {location?.coords.longitude}</Text>
        </View>
    )
}

export default LocationInfo;