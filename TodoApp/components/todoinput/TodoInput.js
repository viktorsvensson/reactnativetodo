
import { useState } from "react";
import { TextInput, Button, View, StyleSheet, Text, TouchableOpacity, Alert, Pressable, Keyboard, UIManager } from "react-native";
import Todo from "../../entities/Todo";
import { findAll, insert } from "../../utils/db";

const TodoInput = ({ setTodoList }) => {

    const [textInput, setTextInput] = useState("")

    const handleChangedText = (text) => {
        setTextInput(text)
    }

    const calculateId = (prevarr) => {
        return prevarr.length ? Math.max(...prevarr.map(todo => todo.id)) + 1 : 1
    }

    const handleClick = async () => {
        Keyboard.dismiss()
        await insert(new Todo(undefined, textInput, false))
        const res = await findAll();
        console.log("findall", res)
        setTodoList(res)
    }

    return (
        <View style={styles.container}>
            <TextInput
                style={styles.textInput}
                placeholder='Input todo'
                value={textInput}
                onChangeText={handleChangedText}
            />
            <Pressable
                style={({ pressed }) => [styles.addButton, { opacity: pressed ? 0.5 : 1.0 }]}
                onPress={handleClick}
            >
                <Text style={styles.addButtonText}>Add</Text>
            </Pressable>
        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    textInput: {
        backgroundColor: '#FFF',
        padding: 10,
        margin: 25,
        width: '60%',
        borderRadius: 5
    },
    addButton: {
        paddingHorizontal: 16,
        paddingVertical: 9,
        backgroundColor: 'gold',
        borderRadius: 5
    },
    addButtonText: {
        //color: '#FFF'
    }
});

export default TodoInput;