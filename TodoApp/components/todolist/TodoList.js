import { useNavigation } from "@react-navigation/native";
import { FlatList, Text, StyleSheet, Dimensions, Pressable, Platform } from "react-native";
import { deleteAll } from "../../utils/db";


const TodoList = ({ todoList }) => {

    const nav = useNavigation()

    const _renderItem = ({ item: todo }) => {

        return (
            <Pressable
                accessible={true}
                accessibilityLabel={`Pressable to view todo number ${todo.id}, ${todo.title}`}
                accessibilityHint={"Clicking this component will navigate to a specific page for the this todo"}
                onPress={() => deleteAll()
                    /*        
                    () => nav.navigate('selectedtodoscreen', {
                    todo
                }) */

                }
                style={styles.todo}
            >
                <Text accessible={false} style={styles.text}>{todo.title}, {todo.id}</Text>
            </Pressable>
        )
    }

    return (
        <FlatList
            accessibilityLiveRegion="assertive"
            data={todoList}
            renderItem={_renderItem}
            numColumns={3}
            columnWrapperStyle={{
                flexWrap: 'wrap',
                justifyContent: 'space-around'
            }}
            contentContainerStyle={{
                minHeight: 400,
                justifyContent: 'space-evenly',
                margin: 10
            }}
        />
    )
}

const styles = StyleSheet.create({
    todo: {
        backgroundColor: '#FFF',
        padding: 10,
        margin: 10,
        borderRadius: 5,
        minWidth: Dimensions.get('window').width * 0.25
    },
    text: {
        textAlign: 'center'
    }
})

export default TodoList;