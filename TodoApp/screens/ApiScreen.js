import { useEffect, useState } from "react";
import { View, Image, Button, Alert } from "react-native"
import { StripeProvider, useStripe } from '@stripe/stripe-react-native'


const ApiScreen = () => {

    const [imgUrl, setImgUrl] = useState()
    const { initPaymentSheet, presentPaymentSheet } = useStripe();
    const [loading, setLoading] = useState(false);

    const BASE_URL = "http://10.0.2.2:3000"

    const fetchPaymentSheetParams = async () => {
        const response = await fetch(`${BASE_URL}/payment-sheet`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
        });
        const { paymentIntent } = await response.json();

        return {
            paymentIntent,
        };
    };

    const initializePaymentSheet = async () => {
        const {
            paymentIntent
        } = await fetchPaymentSheetParams();

        const { error } = await initPaymentSheet({
            merchantDisplayName: "Example, Inc.",
            paymentIntentClientSecret: paymentIntent,
            // Set `allowsDelayedPaymentMethods` to true if your business can handle payment
            //methods that complete payment after a delay, like SEPA Debit and Sofort.
            allowsDelayedPaymentMethods: true,
            defaultBillingDetails: {
                name: 'Jane Doe',
            }
        });
        if (!error) {
            setLoading(true);
        }
    };

    const openPaymentSheet = async () => {
        const { error } = await presentPaymentSheet();

        initializePaymentSheet();
        if (error) {
            Alert.alert(`Error code: ${error.code}`, error.message);
        } else {
            Alert.alert('Success', 'Your order is confirmed!');
        }
    };

    useEffect(() => {
        initializePaymentSheet();
    }, []);

    const getPicture = () => {

        fetch(`${BASE_URL}/apod`)
            .then(res => res.json())
            .then(body => setImgUrl(body.url))

    }

    return (
        <StripeProvider
            publishableKey="pk_test_4QHSdRjQiwkzokPPCiK33eOq"
        >
            <View>
                <Button title="Show Image Of The Day" onPress={getPicture} />
                <Image source={{ uri: imgUrl }} style={{ width: '100%', height: '50%' }}></Image>
                <Button title="Buy a T-shirt with this image" onPress={openPaymentSheet} />
            </View>
        </StripeProvider>
    )
}

export default ApiScreen;