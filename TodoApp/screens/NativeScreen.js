import { View, Text } from "react-native"
import AudioRecorder from "../components/audiorecorder/AudioRecorder";
import CameraComponent from "../components/cameracomponent/CameraComponent";
import LocationInfo from "../components/locationinfo/LocationInfo";


const NativeScreen = () => {

    return (
        <View>
            <LocationInfo />
            <CameraComponent />
            <AudioRecorder />
        </View>
    )
}

export default NativeScreen;