import { useNavigation } from "@react-navigation/native";
import { StyleSheet, View, Text, Pressable, DeviceEventEmitter } from "react-native";
import TodoInput from "../components/todoinput/TodoInput";


const SelectedTodoScreen = ({ route }) => {

    console.log(route.params.todo)

    const { id, title, isDone } = route.params.todo;
    const nav = useNavigation()

    const handleDelete = () => {

        DeviceEventEmitter.emit('removeTodoByIdEvent', id)
        nav.navigate('todosscreen', {});
    }

    return (
        <View style={styles.container}>
            <Text>Title: {title} </Text>
            <Text>Completed: {isDone ? "Yes" : "No"} </Text>
            <Text>Id: {id}</Text>
            <Pressable onPress={handleDelete}>
                <Text>DELETE</Text>
            </Pressable>
        </View>
    )
}

export default SelectedTodoScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})