import { useNavigation } from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';
import { useEffect, useState } from 'react';
import { DeviceEventEmitter, Dimensions, ImageBackground, Pressable, StyleSheet, Text, View } from 'react-native';
import Header from '../components/header/Header';
import TodoInput from '../components/todoinput/TodoInput';
import TodoList from '../components/todolist/TodoList';
import { findAll } from '../utils/db';

export default function TodosScreen() {

  const [todoList, setTodoList] = useState([])
  const nav = useNavigation()

  useEffect(() => {
    DeviceEventEmitter.addListener('removeTodoByIdEvent',
      (id) => setTodoList(prev => prev.filter(todo => todo.id != id)))

    findAll()
      .then(res => setTodoList(res))

    return () => DeviceEventEmitter.removeAllListeners()
  }, [])

  return (

    <ImageBackground
      source={require('../assets/background.jpg')}
      resizeMode='cover'
      style={styles.imagebackground}
    >
      <View style={styles.container}>
        <Header />
        <TodoInput setTodoList={setTodoList}></TodoInput>
        <StatusBar style="auto" />
        <TodoList todoList={todoList} />
        <Pressable onPress={() => nav.navigate('nativescreen')}>
          <Text
            style={{ margin: 10, padding: 10, backgroundColor: '#FFF' }}>
            To native screen
          </Text>
        </Pressable>
      </View>
    </ImageBackground>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: '#DCDCDC',
    alignItems: 'center',
  },
  textInput: {
    backgroundColor: '#FFF',
    padding: 10,
    margin: 25,
    width: '60%',
    borderRadius: 5
  },
  imagebackground: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  }
});
