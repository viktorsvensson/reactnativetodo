import * as SQLite from 'expo-sqlite'
import Todo from '../entities/Todo'

const db = SQLite.openDatabase('todos.db')

export const initDB = () => {
    return new Promise((resolve, reject) => {
        db.transaction((transaction) => {
            transaction.executeSql(
                `CREATE TABLE IF NOT EXISTS todo (
                    id INTEGER PRIMARY KEY,
                    title TEXT NOT NULL,
                    completed BOOLEAN NOT NULL
                )`, [],
                (_, res) => resolve(res),
                (_, err) => reject(err)
            )
        })
    })
}

export const getTableInfo = () => {
    return new Promise((resolve, reject) => {
        db.transaction((transaction) => {
            transaction.executeSql(
                `pragma table_info('todo')`, [],
                (_, res) => resolve(res),
                (_, err) => reject(err)
            )
        })
    })
}

export const insert = (todo) => {

    return new Promise((resolve, reject) => {

        db.transaction((transaction) => {

            transaction.executeSql(
                `INSERT INTO todo (title, completed)
                VALUES (?, ?)`, [todo.title, todo.isCompleted],
                (_, res) => resolve(res),
                (_, err) => reject(err)
            )
        })
    })
}

export const findAll = () => {

    return new Promise((resolve, reject) => {

        db.transaction((transaction) => {

            transaction.executeSql(
                `SELECT * FROM todo`, [],
                (_, res) => resolve(res.rows._array
                    .map(row => new Todo(row.id, row.title, row.completed === 1))),
                (_, err) => reject(err)
            )
        })
    })
}

export const deleteAll = () => {

    return new Promise((resolve, reject) => {

        db.transaction((transaction) => {

            transaction.executeSql(
                `DELETE FROM todo`, [],
                (_, res) => resolve(res),
                (_, err) => reject(err)
            )
        })
    })
}