import Express from 'express';
import 'dotenv/config'
import Stripe from 'stripe';

const server = Express()
server.use(Express.json())

const PORT = 3000;

const stripe = new Stripe(orocess.env.STRIPE_SK_KEY)

server.get("/apod", (req, res) => {

    fetch(`https://api.nasa.gov/planetary/apod?api_key=${process.env.NASA_API_KEY}`)
        .then(response => response.json())
        .then(body => res.json(body))

})

server.post('/payment-sheet', async (req, res) => {

    const paymentIntent = await stripe.paymentIntents.create({
        amount: req.body.amount || 1099,
        currency: 'sek',
        automatic_payment_methods: {
            enabled: true,
        },
    });

    res.json({
        paymentIntent: paymentIntent.client_secret,
    });
});


server.listen(PORT, () => console.log(`Listening on port ${PORT}, key ${process.env.NASA_API_KEY}`))